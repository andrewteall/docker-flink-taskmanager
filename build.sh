#!/bin/sh


# TMPDIR must be contained within the working directory so it is part of the
# Docker context. (i.e. it can't be mktemp'd in /tmp)
TMPDIR=_TMP_

cleanup() {
    rm -rf "${TMPDIR}"
}
trap cleanup EXIT

mkdir -p "${TMPDIR}"

IMAGE_NAME=flink-taskmanaager

FLINK_VERSION=1.2.0
HADOOP_VERSION=27
SCALA_VERSION=2.11

FLINK_BASE_URL="http://www.gtlib.gatech.edu/pub/apache/flink/flink-${FLINK_VERSION}/"
FLINK_DIST_FILE_NAME="flink-${FLINK_VERSION}-bin-hadoop${HADOOP_VERSION}-scala_${SCALA_VERSION}.tgz"
CURL_OUTPUT="${TMPDIR}/${FLINK_DIST_FILE_NAME}"

echo "Downloading ${FLINK_DIST_FILE_NAME} from ${FLINK_BASE_URL}"
curl -s ${FLINK_BASE_URL}${FLINK_DIST_FILE_NAME} --output ${CURL_OUTPUT}

FLINK_DIST="${CURL_OUTPUT}"

docker build --build-arg flink_dist="${FLINK_DIST}" -t "${IMAGE_NAME}" .
